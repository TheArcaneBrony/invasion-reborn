package net.thearcanebrony.invasionreborn.blocks;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.minecraft.block.AbstractFurnaceBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IRecipeHelperPopulator;
import net.minecraft.inventory.IRecipeHolder;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.AbstractCookingRecipe;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.RecipeItemHelper;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.tileentity.*;
import net.minecraft.util.*;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

public class TENexusBlock extends LockableTileEntity
        implements ISidedInventory, IRecipeHolder, IRecipeHelperPopulator, ITickableTileEntity {
    private static final int[] SLOTS_UP = new int[] { 0 };
    private static final int[] SLOTS_DOWN = new int[] { 2, 1 };
    private static final int[] SLOTS_HORIZONTAL = new int[] { 1 };
    protected NonNullList<ItemStack> items = NonNullList.withSize(3, ItemStack.EMPTY);
    private int burnTime;
    private int recipesUsed;
    private int cookTime;
    private int cookTimeTotal;
    protected final IIntArray furnaceData = new IIntArray() {
        public int get(int index) {
            switch (index) {
                case 0:
                    return TENexusBlock.this.burnTime;
                case 1:
                    return TENexusBlock.this.recipesUsed;
                case 2:
                    return TENexusBlock.this.cookTime;
                case 3:
                    return TENexusBlock.this.cookTimeTotal;
                default:
                    return 0;
            }
        }

        public void set(int index, int value) {
            switch (index) {
                case 0:
                    TENexusBlock.this.burnTime = value;
                    break;
                case 1:
                    TENexusBlock.this.recipesUsed = value;
                    break;
                case 2:
                    TENexusBlock.this.cookTime = value;
                    break;
                case 3:
                    TENexusBlock.this.cookTimeTotal = value;
            }

        }

        public int size() {
            return 4;
        }
    };
    private final Object2IntOpenHashMap<ResourceLocation> field_214022_n = new Object2IntOpenHashMap<>();
    //protected final IRecipeType<? extends AbstractCustomRecipe> recipeType;

    public TENexusBlock(IBlockReader a) {
         super(TileEntityType.FURNACE);
    }
    public TENexusBlock(BlockState a) {
         super(TileEntityType.FURNACE);
    }

    public void func_230337_a_(BlockState p_230337_1_, CompoundNBT p_230337_2_) { // TODO: MARK
        // super.func_230337_a_(p_230337_1_, p_230337_2_);
        this.items = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(p_230337_2_, this.items);
        this.burnTime = p_230337_2_.getInt("BurnTime");
        this.cookTime = p_230337_2_.getInt("CookTime");
        this.cookTimeTotal = p_230337_2_.getInt("CookTimeTotal");
        //this.recipesUsed = this.getBurnTime(this.items.get(1));
        CompoundNBT compoundnbt = p_230337_2_.getCompound("RecipesUsed");

        for (String s : compoundnbt.keySet()) {
            this.field_214022_n.put(new ResourceLocation(s), compoundnbt.getInt(s));
        }

    }

    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putInt("BurnTime", this.burnTime);
        compound.putInt("CookTime", this.cookTime);
        compound.putInt("CookTimeTotal", this.cookTimeTotal);
        ItemStackHelper.saveAllItems(compound, this.items);
        CompoundNBT compoundnbt = new CompoundNBT();
        this.field_214022_n.forEach((p_235643_1_, p_235643_2_) -> {
            compoundnbt.putInt(p_235643_1_.toString(), p_235643_2_);
        });
        compound.put("RecipesUsed", compoundnbt);
        return compound;
    }

    public void tick() {
        
    }

    protected boolean canSmelt(@Nullable IRecipe<?> recipeIn) {
        return true;
        // if (!this.items.get(0).isEmpty() && recipeIn != null) {
        //     ItemStack itemstack = recipeIn.getRecipeOutput();
        //     if (itemstack.isEmpty()) {
        //         return false;
        //     } else {
        //         ItemStack itemstack1 = this.items.get(2);
        //         if (itemstack1.isEmpty()) {
        //             return true;
        //         } else if (!itemstack1.isItemEqual(itemstack)) {
        //             return false;
        //         } else if (itemstack1.getCount() + itemstack.getCount() <= this.getInventoryStackLimit()
        //                 && itemstack1.getCount() + itemstack.getCount() <= itemstack1.getMaxStackSize()) { // Forge fix:
        //                                                                                                    // make
        //                                                                                                    // furnace
        //                                                                                                    // respect
        //                                                                                                    // stack
        //                                                                                                    // sizes in
        //                                                                                                    // furnace
        //                                                                                                    // recipes
        //             return true;
        //         } else {
        //             return itemstack1.getCount() + itemstack.getCount() <= itemstack.getMaxStackSize(); // Forge fix:
        //                                                                                                 // make furnace
        //                                                                                                 // respect stack
        //                                                                                                 // sizes in
        //                                                                                                 // furnace
        //                                                                                                 // recipes
        //         }
        //     }
        // } else {
        //     return false;
        // }
    }

    public int[] getSlotsForFace(Direction side) {
        if (side == Direction.DOWN) {
            return SLOTS_DOWN;
        } else {
            return side == Direction.UP ? SLOTS_UP : SLOTS_HORIZONTAL;
        }
    }

    /**
     * Returns true if automation can insert the given item in the given slot from
     * the given side.
     */
    public boolean canInsertItem(int index, ItemStack itemStackIn, @Nullable Direction direction) {
    return true;
        // return this.isItemValidForSlot(index, itemStackIn);
    }

    /**
     * Returns true if automation can extract the given item in the given slot from
     * the given side.
     */
    public boolean canExtractItem(int index, ItemStack stack, Direction direction) {
        if (direction == Direction.DOWN && index == 1) {
            Item item = stack.getItem();
            if (item != Items.WATER_BUCKET && item != Items.BUCKET) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the number of slots in the inventory.
     */
    public int getSizeInventory() {
        return this.items.size();
    }

    public boolean isEmpty() {
        for (ItemStack itemstack : this.items) {
            if (!itemstack.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the stack in the given slot.
     */
    public ItemStack getStackInSlot(int index) {
        return this.items.get(index);
    }

    /**
     * Removes up to a specified number of items from an inventory slot and returns
     * them in a new stack.
     */
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(this.items, index, count);
    }

    /**
     * Removes a stack from the given slot and returns it.
     */
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.items, index);
    }

    /**
     * Sets the given item stack to the specified slot in the inventory (can be
     * crafting or armor sections).
     */
    public void setInventorySlotContents(int index, ItemStack stack) {
        ItemStack itemstack = this.items.get(index);
        boolean flag = !stack.isEmpty() && stack.isItemEqual(itemstack)
                && ItemStack.areItemStackTagsEqual(stack, itemstack);
        this.items.set(index, stack);
        if (stack.getCount() > this.getInventoryStackLimit()) {
            stack.setCount(this.getInventoryStackLimit());
        }

        if (index == 0 && !flag) {
            //this.cookTimeTotal = this.getCookTime();
            this.cookTime = 0;
            this.markDirty();
        }

    }

    /**
     * Don't rename this method to canInteractWith due to conflicts with Container
     */
    public boolean isUsableByPlayer(PlayerEntity player) {
        if (this.world.getTileEntity(this.pos) != this) {
            return false;
        } else {
            return player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D,
                    (double) this.pos.getZ() + 0.5D) <= 64.0D;
        }
    }

    /**
     * Returns true if automation is allowed to insert the given stack (ignoring
     * stack size) into the given slot. For guis use Slot.isItemValid
     */
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if (index == 2) {
            return false;
        } else if (index != 1) {
            return true;
        } else {
            //ItemStack itemstack = this.items.get(1);
            return true;
        }
    }

    public void clear() {
        this.items.clear();
    }

    public void setRecipeUsed(@Nullable IRecipe<?> recipe) {
        if (recipe != null) {
            ResourceLocation resourcelocation = recipe.getId();
            this.field_214022_n.addTo(resourcelocation, 1);
        }

    }

    @Nullable
    public IRecipe<?> getRecipeUsed() {
        return null;
    }

    public void onCrafting(PlayerEntity player) {
    }

    public void func_235645_d_(PlayerEntity p_235645_1_) {
        List<IRecipe<?>> list = this.func_235640_a_(p_235645_1_.world, p_235645_1_.getPositionVec());
        p_235645_1_.unlockRecipes(list);
        this.field_214022_n.clear();
    }

    public List<IRecipe<?>> func_235640_a_(World p_235640_1_, Vector3d p_235640_2_) {
        List<IRecipe<?>> list = Lists.newArrayList();

        for (Object2IntMap.Entry<ResourceLocation> entry : this.field_214022_n.object2IntEntrySet()) {
            p_235640_1_.getRecipeManager().getRecipe(entry.getKey()).ifPresent((p_235642_4_) -> {
                list.add(p_235642_4_);
                func_235641_a_(p_235640_1_, p_235640_2_, entry.getIntValue(),
                        ((AbstractCookingRecipe) p_235642_4_).getExperience());
            });
        }

        return list;
    }

    /***
     * Spawn XP
     * 
     * @param p_235641_0_ World
     * @param p_235641_1_ Position
     * @param p_235641_2_ ?? base xp ??
     * @param p_235641_3_ ?? multiplier ??
     */
    private static void func_235641_a_(World p_235641_0_, Vector3d p_235641_1_, int p_235641_2_, float p_235641_3_) {
        int i = MathHelper.floor((float) p_235641_2_ * p_235641_3_);
        float f = MathHelper.frac((float) p_235641_2_ * p_235641_3_);
        if (f != 0.0F && Math.random() < (double) f) {
            ++i;
        }

        while (i > 0) {
            int j = ExperienceOrbEntity.getXPSplit(i);
            i -= j;
            p_235641_0_.addEntity(new ExperienceOrbEntity(p_235641_0_, p_235641_1_.x, p_235641_1_.y, p_235641_1_.z, j));
        }

    }

    public void fillStackedContents(RecipeItemHelper helper) {
        for (ItemStack itemstack : this.items) {
            helper.accountStack(itemstack);
        }

    }

    net.minecraftforge.common.util.LazyOptional<? extends net.minecraftforge.items.IItemHandler>[] handlers = net.minecraftforge.items.wrapper.SidedInvWrapper
            .create(this, Direction.UP, Direction.DOWN, Direction.NORTH);

    @Override
    public <T> net.minecraftforge.common.util.LazyOptional<T> getCapability(
            net.minecraftforge.common.capabilities.Capability<T> capability, @Nullable Direction facing) {
        if (!this.removed && facing != null
                && capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            if (facing == Direction.UP)
                return handlers[0].cast();
            else if (facing == Direction.DOWN)
                return handlers[1].cast();
            else
                return handlers[2].cast();
        }
        return super.getCapability(capability, facing);
    }

    /**
     * invalidates a tile entity
     */
    @Override
    public void remove() {
        super.remove();
        for (int x = 0; x < handlers.length; x++)
            handlers[x].invalidate();
    }

    @Override
    protected ITextComponent getDefaultName() {
        // TODO Auto-generated method stub
        return new TranslationTextComponent("Nexus, bane of your existance.");
    };

    @Override
    protected Container createMenu(int id, PlayerInventory player) {
        return new Container(ContainerType.FURNACE, id) {

            @Override
            public boolean canInteractWith(PlayerEntity playerIn) {
                // TODO Auto-generated method stub
                //func_235641_a_(playerIn.world, playerIn.getPositionVec(), 1000, 1000);
                System.out.println("canInteractWith triggered!");
                return true;
            }

        };
    }
}