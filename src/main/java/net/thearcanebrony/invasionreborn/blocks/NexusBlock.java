package net.thearcanebrony.invasionreborn.blocks;

import javax.annotation.Nullable;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ContainerBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class NexusBlock extends Block {
    public static final String NAME = "Nexus";

    public NexusBlock() {
        super(AbstractBlock.Properties.create(Material.ROCK));

    }

    public BlockRenderType getRenderType(final BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    public ActionResultType onBlockActivated(final BlockState state, final World world, final BlockPos pos,
            final PlayerEntity player, final Hand handIn, final BlockRayTraceResult hit) {
        if (!world.isRemote) {
            final INamedContainerProvider tileEntity = getContainer(state, world, pos);
            if (tileEntity != null) {
                NetworkHooks.openGui((ServerPlayerEntity) player, tileEntity, pos);

            }
        }
        return ActionResultType.SUCCESS;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        // builder.add(FACING,LIT);
    }

    @Override
    public boolean hasTileEntity(final BlockState state) {
        return true;
    }

    @Nullable
    @Override
    public INamedContainerProvider getContainer(final BlockState state, final World worldIn, final BlockPos pos) {
        return (INamedContainerProvider) worldIn.getTileEntity(pos);
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader worldIn) { //ERROR: The method createTileEntity(Blockstate) of type NeuxBlock must override or implement a supertype method
        return new TENexusBlock(state);
    }
}
