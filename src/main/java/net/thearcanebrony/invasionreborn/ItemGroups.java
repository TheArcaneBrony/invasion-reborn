package net.thearcanebrony.invasionreborn;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;


@Mod.EventBusSubscriber( bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ItemGroups {
    public static ItemGroup ITEM_GROUP= new ItemGroup("Invasion Reborn") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(IBlocks.NEXUSBLOCK.get());
        }
    };
}