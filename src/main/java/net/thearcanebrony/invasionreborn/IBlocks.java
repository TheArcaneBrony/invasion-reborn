package net.thearcanebrony.invasionreborn;

import net.minecraft.block.Block;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.thearcanebrony.invasionreborn.blocks.CNexusBlock;
import net.thearcanebrony.invasionreborn.blocks.NexusBlock;
import net.thearcanebrony.invasionreborn.gui.GUINexusBlock;

import static net.thearcanebrony.invasionreborn.InvasionMod.MODID;

public class IBlocks {
        private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MODID);
        private static final DeferredRegister<Item> BLOCKITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MODID);
        private static final DeferredRegister<TileEntityType<?>> TILES = DeferredRegister
                        .create(ForgeRegistries.TILE_ENTITIES, MODID);
        private static final DeferredRegister<ContainerType<?>> CONTAINERS = DeferredRegister
                        .create(ForgeRegistries.CONTAINERS, MODID);

        public static void setup() {
                BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
                TILES.register(FMLJavaModLoadingContext.get().getModEventBus());
                CONTAINERS.register(FMLJavaModLoadingContext.get().getModEventBus());
                BLOCKITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());

                DeferredWorkQueue.runLater(() -> {
                        ScreenManager.registerFactory(NEXUS.get(), GUINexusBlock::new);
                });
        }

        public static final RegistryObject<NexusBlock> NEXUSBLOCK = BLOCKS.register("nexus", NexusBlock::new);
        public static final RegistryObject<Item> NEXUSBLOCK_ITEM = BLOCKITEMS.register("nexus",
                        () -> new BlockItem(NEXUSBLOCK.get(), new Item.Properties().group(ItemGroups.ITEM_GROUP)));

	public static final RegistryObject<ContainerType<CNexusBlock>> NEXUS = CONTAINERS.register("nexus", () -> IForgeContainerType.create(CNexusBlock::new));
}